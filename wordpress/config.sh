#!/bin/sh

sed -i "s/\(define( 'DB_NAME', '\).*\(' );\)/\1${DB_NAME:-wordpress11}\2/" /wordpress/wp-config.php
sed -i "s/\(define( 'DB_USER', '\).*\(' );\)/\1${DB_USER:-username}\2/" /wordpress/wp-config.php
sed -i "s/\(define( 'DB_PASSWORD', '\).*\(' );\)/\1${DB_PASSWORD:-password}\2/" /wordpress/wp-config.php
sed -i "s/\(define( 'DB_HOST', '\).*\(' );\)/\1${DB_HOST:mariadb}\2/" /wordpress/wp-config.php
