<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'username' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password' );

/** MySQL hostname */
define( 'DB_HOST', 'mariadb' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'uOq$i~B73F`|NHnO7NI4%Yz39ZC/4ygU+P[1]k8v&s8S47ZFDwr->fbU* jTaI1Q' );
define( 'SECURE_AUTH_KEY',  'A{mxprM^&~?&]a,%Y 4!/RQwEg%Z&t|Gi/Vt9NF:WA(S!srsZ@FN#OX,qS)F4;Q.' );
define( 'LOGGED_IN_KEY',    'v$5a92ie$Qgb|1~[H!UhLZHYj;|U,-S{.r1t;5z^fy)YQFRw=;c]{%:J-MIW&0*u' );
define( 'NONCE_KEY',        'jQo&hA?2*?4}6+2z7QvQq3Kgty!lrQjmsdRX8Ar7S;0T|&^OCDFi7W{1)tB4@1|}' );
define( 'AUTH_SALT',        'g~k3(SUUGMGa:DM;$P)is5%,#M/`4$PyiTiCs[+PtgN/e|/!.dZQX.|z(~SR<=7v' );
define( 'SECURE_AUTH_SALT', '$kf+Y1HjfjD0 ?{&V2xv+~+XWr.<$^zcE$q}^%DtQih`x) F1m4ECi+@Y{Ht>*?%' );
define( 'LOGGED_IN_SALT',   'EP,~BQe0fAeq:l=R v{>n:+E&)~G*=)Tcq[QBZS-K|tb{duFt 7p(,HR%)^&Apd/' );
define( 'NONCE_SALT',       '/[7]l>a)8kg42Y>|.@az[/0)j;AlLCLA4j3!tR.}g3rT}>V/(_3Sl0z3#,tT@x<p' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );